package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }
    
    @Test
    public void testBoxCompleteFalse() {
        //Creating a box that should be incorrect and checking if it returns false
        DotsAndBoxesGrid boxCompleteFalse = new DotsAndBoxesGrid(5, 5, 2);
        boxCompleteFalse.drawHorizontal(0, 0, 0);
        boxCompleteFalse.drawVertical(0, 0, 1);
        boxCompleteFalse.drawHorizontal(0, 1, 0);
        boxCompleteFalse.drawVertical(1, 1, 1);

        assertFalse(boxCompleteFalse.boxComplete(0, 0));
    }

    @Test
    public void testBoxCompleteTrue() {
        //Creating a box that should be correct and checking if it returns true
        DotsAndBoxesGrid boxCompleteTrue = new DotsAndBoxesGrid(5, 5, 2);
        boxCompleteTrue.drawHorizontal(0, 0, 0);
        boxCompleteTrue.drawVertical(0, 0, 1);
        boxCompleteTrue.drawHorizontal(0, 1, 0);
        boxCompleteTrue.drawVertical(1, 0, 1);

        assertTrue(boxCompleteTrue.boxComplete(0, 0));
    }

    @Test
    public void testDrawHorizontalDrawn() {
        //Draws a horizontal line, then draws another horizontal line in the same
        //place to check if an exception is appropriately thrown
        DotsAndBoxesGrid horizontalDrawn = new DotsAndBoxesGrid(5, 5, 1);
        horizontalDrawn.drawHorizontal(0, 0, 1);
        assertThrows(RuntimeException.class, () -> {
            horizontalDrawn.drawHorizontal(0, 0, 1);
        });
    }

    @Test
    public void testDrawHorizontalNotDrawn() {
        //Draws a horizontal line to check if no exception is thrown
        DotsAndBoxesGrid horizontalNotDrawn = new DotsAndBoxesGrid(5, 5, 1);
        assertDoesNotThrow(() -> {
            horizontalNotDrawn.drawHorizontal(0, 0, 1);
        });
    }

    @Test
    public void testDrawVerticalDrawn() {
        //Draws a vertical line, then draws another vertical line in the same
        //place to check if an exception is appropriately thrown
        DotsAndBoxesGrid verticalDrawn = new DotsAndBoxesGrid(5, 5, 1);
        verticalDrawn.drawVertical(0, 0, 1);
        assertThrows(RuntimeException.class, () -> {
            verticalDrawn.drawVertical(0, 0, 1);
        });
    }

    @Test
    public void testDrawVerticalNotDrawn() {
        //Draws a vertical line to check if no exception is thrown
        DotsAndBoxesGrid verticalNotDrawn = new DotsAndBoxesGrid(5, 5, 1);
        assertDoesNotThrow(() -> {
            verticalNotDrawn.drawVertical(0, 0, 1);
        });    
    }
}
